import os
from dotenv import load_dotenv
from google.cloud import storage
from google.cloud.storage import constants


def get_storage_client(service_key=None):

    if service_key:
        storage_client = storage.Client.from_service_account_json(service_key)
    else:
        storage_client = storage.Client()

    return storage_client

def load_env(env_file):
    load_dotenv(env_file)


def setup(env_file="qa.env", service_key=True):
    if service_key:
        load_env(env_file)
    service_key = os.environ.get('TYPE')
    return get_storage_client(service_key)


def change_storage_class(storage_client, bucket_name, storage_class):
    bucket = storage_client.get_bucket(bucket_name)
    bucket.storage_class = storage_class
    bucket.patch()
    print(bucket_name, bucket.storage_class)

def main(client):
    # bucket_name = "MayankDemoBucket"
    bucket_name = "kmsgbzyuug"
    storage_class = constants.NEARLINE_STORAGE_CLASS
    change_storage_class(client, bucket_name, storage_class)

if __name__ == "__main__":
    env_file = "qa.env"
    service_key = True
    client = setup(env_file, service_key)
    main(client)
