package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"cloud.google.com/go/storage"
	"github.com/joho/godotenv"
)

// Function reads
func loadEnv(envFile string) {
	err := godotenv.Load(envFile)
	if err != nil {
		log.Fatalf("Some error occured. Err: %s", err)
	}

	//	os.Getenv("GOOGLE_APPLICATION_CREDENTIALS")
}

// Function to check error, log it and exit.
func LogError(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func NewStorageClient() (*storage.Client, *context.Context, error) {
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		log.Fatal(err)
	}
	return client, &ctx, err
}

func DeleteBuckets(client *storage.Client, ctx *context.Context, projectID string, bucketName string) error {
	bucket := client.Bucket(bucketName)
	if err := bucket.Delete(*ctx); err != nil {
		return fmt.Errorf("Bucket(%q).Delete: %v", bucketName, err)
	}
	return nil
}

func main() {

	loadEnv("qa.env")
	projectID := os.Getenv("PROJECT_ID")
	bucketName := "xgcugfvrps"
	client, ctx, err := NewStorageClient()
	defer client.Close()
	LogError(err)
	// Add Code below for various operation on buckets
	DeleteBuckets(client, ctx, projectID, bucketName)

}
