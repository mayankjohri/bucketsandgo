import os
from dotenv import load_dotenv
from google.cloud import storage

def get_storage_client(service_key=None):

    if service_key:
        storage_client = storage.Client.from_service_account_json(service_key)
    else:
        storage_client = storage.Client()

    return storage_client

def load_env(env_file):
    load_dotenv(env_file)


def setup(env_file="qa.env", service_key=True):
    if service_key:
        load_env(env_file)
    service_key = os.environ.get('TYPE')
    return get_storage_client(service_key)

def delete_bucket(storage_client, bucket_name):
    # bucket = storage_client.get_bucket(bucket_name)
    bucket = storage_client.lookup_bucket(bucket_name)
    if bucket:
        bucket.delete()
        print(f"Deleted {bucket.name}")
    else:
        print("Good bucket was scared thus alreaayd left.")


def main(client):
    # bucket_name = "MayankDemoBucket"
    bucket_name = "mayankdemobucket"
    delete_bucket(client, bucket_name)

if __name__ == "__main__":
    env_file = "qa.env"
    service_key = True
    client = setup(env_file, service_key)
    main(client)
