import os
from dotenv import load_dotenv
from google.cloud import storage

def get_storage_client(service_key=None):

    if service_key:
        storage_client = storage.Client.from_service_account_json(service_key)
    else:
        storage_client = storage.Client()

    return storage_client

def load_env(env_file):
    load_dotenv(env_file)


def setup(env_file="qa.env", service_key=True):
    if service_key:
        load_env(env_file)
    service_key = os.environ.get('TYPE')
    return get_storage_client(service_key)

def bucket_info(storage_client, bucket_name):
    bucket = storage_client.lookup_bucket(bucket_name)
    if bucket:

        blob = bucket.get_blob(blob_name)

        print("Blob: {}".format(blob.name))
        print("Bucket: {}".format(blob.bucket.name))
        print("Storage class: {}".format(blob.storage_class))
        print("ID: {}".format(blob.id))
        print("Size: {} bytes".format(blob.size))
        print("Updated: {}".format(blob.updated))
        print("Generation: {}".format(blob.generation))
        print("Metageneration: {}".format(blob.metageneration))
        print("Etag: {}".format(blob.etag))
        print("Owner: {}".format(blob.owner))
        print("Component count: {}".format(blob.component_count))
        print("Crc32c: {}".format(blob.crc32c))
        print("md5_hash: {}".format(blob.md5_hash))
        print("Cache-control: {}".format(blob.cache_control))
        print("Content-type: {}".format(blob.content_type))
        print("Content-disposition: {}".format(blob.content_disposition))
        print("Content-encoding: {}".format(blob.content_encoding))
        print("Content-language: {}".format(blob.content_language))
        print("Metadata: {}".format(blob.metadata))
        print("Medialink: {}".format(blob.media_link))
        print("Custom Time: {}".format(blob.custom_time))
        print("Temporary hold: ", "enabled" if blob.temporary_hold else "disabled")
        print(
            "Event based hold: ",
            "enabled" if blob.event_based_hold else "disabled",
        )
        if blob.retention_expiration_time:
            print(
                "retentionExpirationTime: {}".format(
                    blob.retention_expiration_time
                )
            )

    else:
        print("Sorry not able to find the bucket")

def main(client):
    # bucket_name = "MayankDemoBucket"
    bucket_name = "kmsgbzyuug"
    # bucket_name = "kmsgbzyuug123"
    bucket_info(client, bucket_name)

if __name__ == "__main__":
    env_file = "qa.env"
    service_key = True
    client = setup(env_file, service_key)
    main(client)
