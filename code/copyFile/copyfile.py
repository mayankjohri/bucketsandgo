from dotenv import load_dotenv
import google.auth
from google.cloud import storage


load_dotenv("qa.env")


creds, projectPid = google.auth.default()

def copy_file(src_file, dst_file):
    src_bucket, src_file = src_file.split("gs://")[1].split("/", 1)
    print(src_bucket)

    dst_bucket, dst_file = dst_file.split("gs://")[1].split("/", 1)[0]
    print(dst_bucket)
    client = storage.Client()

    src_bucket = client.get_bucket(src_bucket)
    dst_bucket = client.get_bucket(dst_bucket)
    src_blob = src_bucket.get_blob(src_file)
    dst_blob = dst_bucket.get_blob(dst_file)
    with open(src_file) as fp:
        print(fp)



src_file = "gs://euvnrzavgd/generate.vim"
dst_file = "gs://kmsgbzyuug/dummy/generate.vim"
copy_file(src_file, dst_file)
