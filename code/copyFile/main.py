import os
from dotenv import load_dotenv
from google.cloud import storage
from google.cloud.storage import constants


def get_storage_client(service_key=None):

    if service_key:
        storage_client = storage.Client.from_service_account_json(service_key)
    else:
        storage_client = storage.Client()

    return storage_client

def load_env(env_file):
    load_dotenv(env_file)


def setup(env_file="qa.env", service_key=True):
    if service_key:
        load_env(env_file)
    service_key = os.environ.get('TYPE')
    return get_storage_client(service_key)


def upload_file(storage_client, bucket_name, src_file, dst_loc):
    bucket = storage_client.get_bucket(bucket_name)
    file_name = os.path.basename(src_file)
    dst_loc = "{dst_loc}/{file_name}"
    blob = bucket.blob(dst_loc)
    blob.upload_from_filename(src_file)


def upload_file_using_string(storage_client, bucket_name, src_data, dst_file):
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(dst_file)
    blob.upload_from_string(src_data)


def main(client):
    # bucket_name = "MayankDemoBucket"
    bucket_name = "kmsgbzyuug"
    src_file =  "dummy.txt"
    src_data = """Nobody should be restricted by the software they use. There
    are four freedoms that every user should have:

    - the freedom to use the software for any purpose,
    - the freedom to change the software to suit your needs,
    - the freedom to share the software with your friends and neighbors, and
    - the freedom to share the changes you make.
    """
    upload_file(client, bucket_name, src_file, "test/1")
    upload_file_using_string(client, bucket_name,
            src_file, "test/mem_data.txt")

if __name__ == "__main__":
    env_file = "qa.env"
    service_key = True
    client = setup(env_file, service_key)
    main(client)
