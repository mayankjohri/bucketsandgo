package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"

	"cloud.google.com/go/storage"
	"github.com/joho/godotenv"
)

// Function reads
func loadEnv(envFile string) {
	err := godotenv.Load(envFile)
	if err != nil {
		log.Fatalf("Some error occured. Err: %s", err)
	}
}

// Function to check error, log it and exit.
func LogError(err error, message string) {
	if err != nil {
		result := fmt.Sprintf("%s due to error: %s", message, err)
		log.Fatalln(result)
	}
}

func NewStorageClient() (*storage.Client, *context.Context, error) {
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		log.Fatal(err)
	}
	return client, &ctx, err
}

func CopyFile(client *storage.Client, ctx *context.Context,
	bucketName string, srcFileName string,
	dstFileName string) error {
	bucket := client.Bucket(bucketName)
	fp, err := os.Open(srcFileName)
	if err != nil {
		LogError(err,
			"Sorry, I am not able to read data from file "+srcFileName)
	}

	defer fp.Close()
	obj := bucket.Object(dstFileName)
	wc := obj.NewWriter(*ctx)
	if _, err = io.Copy(wc, fp); err != nil {
		return fmt.Errorf("io.Copy: %v", err)
	}
	if err = wc.Close(); err != nil {
		fmt.Println("Error", err)
		return fmt.Errorf("Writer.Close: %v", err)
	}
	return nil
}

func main() {

	loadEnv("qa.env")
	// projectID := os.Getenv("PROJECT_ID")
	bucketName := "kmsgbzyuug"
	client, ctx, err := NewStorageClient()
	defer client.Close()
	LogError(err, "Failed to create Client")
	// Add Code below for various operation on buckets
	CopyFile(client, ctx, bucketName, "copyfile.py", "test/copyfile.py")
}
