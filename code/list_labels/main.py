import os
from dotenv import load_dotenv
from google.cloud import storage

def get_storage_client(service_key=None):

    if service_key:
        storage_client = storage.Client.from_service_account_json(service_key)
    else:
        storage_client = storage.Client()

    return storage_client

def load_env(env_file):
    load_dotenv(env_file)


def setup(env_file="qa.env", service_key=True):
    if service_key:
        load_env(env_file)
    service_key = os.environ.get('TYPE')
    return get_storage_client(service_key)

def add_bucket_labels(storage_client, bucket_name):
    bucket= storage_client.lookup_bucket(bucket_name)
    if bucket:
        labels = bucket.labels
        labels["test_001"] = "testing001"
        bucket.labels = labels
        bucket.patch()

    else:
        print("Sorry not able to find the bucket")


def list_bucket_labels(storage_client, bucket_name):
    bucket= storage_client.lookup_bucket(bucket_name)
    if bucket:
        print(f"bucket labels: {bucket.labels}")
        for label in bucket.labels:
            print(label)
    else:
        print("Sorry not able to find the bucket")

def main(client):
    # bucket_name = "MayankDemoBucket"
    bucket_name = "kmsgbzyuug"
    # bucket_name = "kmsgbzyuug123"
    add_bucket_labels(client, bucket_name)
    list_bucket_labels(client, bucket_name)

if __name__ == "__main__":
    env_file = "qa.env"
    service_key = True
    client = setup(env_file, service_key)
    main(client)
