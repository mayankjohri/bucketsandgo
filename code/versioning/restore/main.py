import os
from dotenv import load_dotenv
from google.cloud import storage

def get_storage_client(service_key=None):

    if service_key:
        storage_client = storage.Client.from_service_account_json(service_key)
    else:
        storage_client = storage.Client()

    return storage_client

def load_env(env_file):
    load_dotenv(env_file)


def setup(env_file="qa.env", service_key=True):
    if service_key:
        load_env(env_file)
    service_key = os.environ.get('TYPE')
    return get_storage_client(service_key)

def bucket_info(storage_client, bucket_name):
    bucket = storage_client.lookup_bucket(bucket_name)
    if bucket:
        print(f"Versioning Enabled: {bucket.versioning_enabled}")
        bucket.versioning_enabled = True
        bucket.patch()
        
        print(f"Versioning Enabled: {bucket.versioning_enabled}")
        """
        bucket.versioning_enabled = False
        bucket.patch()
        """
        print(f"Versioning Enabled: {bucket.versioning_enabled}")
    else:
        print("Sorry not able to find the bucket")


def activate_version_file(storage_client, bucket_name, file_path, generation):
    
    source_bucket = storage_client.bucket(bucket_name)
    source_blob = source_bucket.blob(file_path)

    blob_copy = source_bucket.copy_blob(
        source_blob, source_bucket, file_path, source_generation=generation
    )

def delete_file_archived_generation(storage_client,
        bucket_name, blob_name, generation):
    bucket = storage_client.get_bucket(bucket_name)
    bucket.delete_blob(blob_name, generation=generation)



def main(client):
    # bucket_name = "MayankDemoBucket"
    bucket_name = "kmsgbzyuug"
    # bucket_name = "kmsgbzyuug123"
    bucket_info(client, bucket_name)
    activate_version_file(client, "kmsgbzyuug", "dummy/generate.vim", "1650038894459956")
    delete_file_archived_generation(client, "kmsgbzyuug",
            "dummy/generate.vim", "1650038894459956")

if __name__ == "__main__":
    env_file = "qa.env"
    service_key = True
    client = setup(env_file, service_key)
    main(client)
