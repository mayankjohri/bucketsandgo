import json
import os
import time
import jwt

from google.auth.transport import requests
from google.oauth2 import service_account
from dotenv import load_dotenv

def read_key_file(key_file):
    with open(key_file) as fp:
        return json.load(fp)

"""
def get_session(key_file, scope):
    creds = service_account.Credentials.from_service_account_file(
            key_file,
            scopes=[scope])
    key_data = read_key_file(key_file)
    session = requests.AuthorizedSession(creds)
    st_time = time.time()
    exp_time = st_time + 3600  # When should the session be expired
    payload = {
            "iss": key_data['client_email'],
            "sub": key_data['client_email'],
            "aud": scope,
            "iat": st_time,
            "exp": exp_time
            }
    additional_headers = {"kid": key_data['private_key_id']}
    signed_jwt = jwt.encode(payload, key_data['private_key'],
            header=additional_headers) # , algorithm='RS256')
    creds.token = signed_jwt
    session.headers.update({"Authorization": f"Bearer {creds.token}"})
    return creds, session
"""
def get_session(key_file, scope):
    creds = service_account.Credentials.from_service_account_file(
            key_file,
            scopes=[scope])
    key_data = read_key_file(key_file)
    iat = time.time()
    exp = iat + 3600
    payload = {
            "iss": key_data['client_email'],
            "sub": key_data['client_email'],
            "aud": scope,
            'iat': iat,
            'exp': exp}
    additional_headers = {"kid": key_data['private_key_id']}
    # additional_headers = {'kid': PRIVATE_KEY_ID_FROM_JSON}
    signed_jwt = jwt.encode(payload,
                            key_data['private_key'],
                            headers=additional_headers,
                            algorithm='RS256')

    creds.token = signed_jwt
    request = requests.Request()
    print(creds.token)
    print(dir(creds))
    session = requests.AuthorizedSession(creds)
    session.headers.update({"Authorization": f"Bearer {creds.token}"})
    return creds, session, request

def is_versioning_enabled(bucket_name, key_file, scope):
    creds, session, request = get_session(key_file, scope)
    print(session.headers)
    headers = {
            "Content-Type": "application/json;charset=utf-8",
            "Authorization": f"Bearer {creds.token}"}
    at = creds.token
    url = f"{scope}storage/v1/b/{bucket_name}?fields=versioning?access_token={at}"
    print(url)
    print(session.headers)
    response = session.request("GET", url, headers=headers)
    print(response)


if __name__ == "__main__":
    scope = "https://storage.googleapis.com/"
    load_dotenv("qa.env")
    key_file = os.environ.get("GOOGLE_APPLICATION_CREDENTIALS")
    is_versioning_enabled("kmsgbzyuug", key_file, scope)
