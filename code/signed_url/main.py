import os
from datetime import timedelta
from dotenv import load_dotenv
from google.cloud import storage
from google.oauth2 import service_account

def get_storage_client(service_key=None):

    if service_key:
        storage_client = storage.Client.from_service_account_json(service_key)
    else:
        storage_client = storage.Client()

    return storage_client

def load_env(env_file):
    load_dotenv(env_file)


def setup(env_file="qa.env", service_key=True):
    if service_key:
        load_env(env_file)
    service_key = os.environ.get('TYPE')
    return get_storage_client(service_key)

def create_signed_url(storage_client,
                      bucket_name,
                      bucket_filename,
                      keyfile):
    bucket = storage_client.lookup_bucket(bucket_name)
    if bucket:
        blob = bucket.blob(bucket_filename)
        if blob.exists():
            print("Lets create signed_url")
            expiration = timedelta(3) # valid for 3 days
            credentials = service_account.Credentials.from_service_account_file(keyfile)
            expiration = timedelta(3) # valid for 3 days
            url = blob.generate_signed_url(expiration, method="GET",
                                           credentials=credentials)
            print(url)
    else:
        print("Sorry not able to find the bucket")

def main(client):
    # bucket_name = "MayankDemoBucket"
    # bucket_name = "kmsgbzyuug"
    bucket_name = "kmsgbzyuug"
    bucket_file = "test/copyfile.py"
    jsonfile = os.getenv("GOOGLE_APPLICATION_CREDENTIALS")
    create_signed_url(client, bucket_name, bucket_file, jsonfile)

if __name__ == "__main__":
    env_file = "qa.env"
    service_key = True
    client = setup(env_file, service_key)
    main(client)
