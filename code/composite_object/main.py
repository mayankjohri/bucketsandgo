import os
from dotenv import load_dotenv
from google.cloud import storage

def get_storage_client(service_key=None):

    if service_key:
        storage_client = storage.Client.from_service_account_json(service_key)
    else:
        storage_client = storage.Client()

    return storage_client

def load_env(env_file):
    load_dotenv(env_file)


def setup(env_file="qa.env", service_key=True):
    if service_key:
        load_env(env_file)
    service_key = os.environ.get('TYPE')
    return get_storage_client(service_key)

def composite_obj(storage_client, bucket_name, dest_blob_name, files):
    bucket = storage_client.lookup_bucket(bucket_name)
    if bucket:
        destination = bucket.blob(dest_blob_name)
        destination.content_type = "text/plain"
        sources = []
        for file in files:
            sources.append(bucket.get_blob(file))
        destination.compose(sources)

    else:
        print("Sorry not able to find the bucket")

def main(client):
    bucket_name = "kmsgbzyuug"
    files = ['test/tdd/copyfile.py', 'dummy/generate.vim']
    dest_file = "test/final.txt"
    composite_obj(client, bucket_name, dest_file, files)

if __name__ == "__main__":
    env_file = "qa.env"
    service_key = True
    client = setup(env_file, service_key)
    main(client)
