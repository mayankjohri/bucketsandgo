import os
from dotenv import load_dotenv
from google.cloud import storage

def get_storage_client(service_key=None):

    if service_key:
        storage_client = storage.Client.from_service_account_json(service_key)
    else:
        storage_client = storage.Client()

    return storage_client

def load_env(env_file):
    load_dotenv(env_file)


def setup(env_file="qa.env", service_key=True):
    if service_key:
        load_env(env_file)
    service_key = os.environ.get('TYPE')
    return get_storage_client(service_key)

def create_bucket(client, name, storage_class, location="us"):
    bucket = client.bucket(name)
    bucket.storage_class = storage_class
    print(f"Creating bucket {name}")
    new_bucket = client.create_bucket(bucket, location=location)
    return new_bucket

def main(client):
    # bucket_name = "MayankDemoBucket"
    bucket_name = "mayankdemobucket"
    bucket = create_bucket(client, bucket_name, "COLDLINE")
    print(bucket.name, bucket.location, bucket.storage_class )

if __name__ == "__main__":
    env_file = "qa.env"
    service_key = True
    client = setup(env_file, service_key)
    main(client)
