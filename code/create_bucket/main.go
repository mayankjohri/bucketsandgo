package main

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"

	"cloud.google.com/go/storage"
	"github.com/joho/godotenv"
)

//Common Functions

const charset = "abcdefghijklmnopqrstuvwxyz"

var seededRand *rand.Rand = rand.New(
	rand.NewSource(time.Now().UnixNano()))

func RandomString(length int, charset string) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

// Function reads
func loadEnv(envFile string) {
	err := godotenv.Load(envFile)
	if err != nil {
		log.Fatalf("Some error occured. Err: %s", err)
	}

	os.Getenv("GOOGLE_APPLICATION_CREDENTIALS")
}

// Function to check error, log it and exit.
func LogError(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func NewStorageClient() (*storage.Client, *context.Context, error) {
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		log.Fatal(err)
	}
	return client, &ctx, err
}

func CreateBucket(projectID, bucketName string) error {
	fmt.Println("Starting the process to create the bucket")
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		return fmt.Errorf("Sorry, failed to create Storage Client: %v", err)
	}
	defer client.Close()

	storageClassAndLocation := &storage.BucketAttrs{
		StorageClass: "COLDLINE",
		Location:     "asia",
	}
	bucket := client.Bucket(bucketName)
	if err := bucket.Create(ctx, projectID, storageClassAndLocation); err != nil {
		return fmt.Errorf("Bucket(%q).Create: %v", bucketName, err)
	}
	fmt.Println("Created bucket %v in %v with storage class %v\n",
		bucketName,
		storageClassAndLocation.Location,
		storageClassAndLocation.StorageClass)
	return nil
}

func main() {
	loadEnv("qa.env")
	projectID := os.Getenv("PROJECT_ID")
	client, ctx, err := NewStorageClient()
	defer client.Close()
	LogError(err)

	// Add Code below for various operation on buckets
	fmt.Println(ctx)
	bucketName := RandomString(10, charset)
	err = CreateBucket(projectID, bucketName)
	if err != nil {
		fmt.Println(err)
	}
}
