// buckets project main.go
package main

import (
	"context"
	"fmt"

	// "io"
	"log"
	"os"

	"time"

	"cloud.google.com/go/storage"
	"google.golang.org/api/iterator"
	"google.golang.org/api/option"

	"github.com/joho/godotenv"
)

func loadEnv() string {
	err := godotenv.Load("test.env")
	if err != nil {
		log.Fatalf("Some error occured. Err: %s", err)
	}

	val := os.Getenv("GOOGLE_APPLICATION_CREDENTIALS")
	fmt.Println(val)
	return val
}

func CreateBucket(projectID, bucketName string) error {
	fmt.Println("Starting the process to create the bucket")
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		return fmt.Errorf("storage.NewClient: %v", err)
	}
	defer client.Close()

	ctx, cancel := context.WithTimeout(ctx, time.Second*160)
	defer cancel()

	storageClassAndLocation := &storage.BucketAttrs{
		StorageClass: "COLDLINE",
		Location:     "asia",
	}
	bucket := client.Bucket(bucketName)
	if err := bucket.Create(ctx, projectID, storageClassAndLocation); err != nil {
		return fmt.Errorf("Bucket(%q).Create: %v", bucketName, err)
	}
	fmt.Println("Created bucket %v in %v with storage class %v\n", bucketName, storageClassAndLocation.Location, storageClassAndLocation.StorageClass)
	return nil
}

func explicitUsingJSONKeys(jsonPath string, projectID string) (*storage.Client, *context.Context, error) {
	ctx := context.Background()
	client, err := storage.NewClient(ctx, option.WithCredentialsFile(jsonPath))
	if err != nil {
		log.Fatal(err)
	}
	return client, &ctx, err
}

func ListBuckets(client *storage.Client, ctx *context.Context, projectID string) {

	fmt.Println("Current Buckets:")
	it := client.Buckets(*ctx, projectID)
	for {
		battrs, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(battrs.Name)
	}
}
func main() {
	fmt.Println("Hello World!")
	projectID := "sonic-wonder-334710"
	fileLoc := loadEnv()
	client, ctx, err := explicitUsingJSONKeys(fileLoc, projectID)

	defer client.Close()
	fmt.Println(client)
	err = CreateBucket(projectID, "samplebucket101219910")
	ListBuckets(client, ctx, projectID)
	fmt.Println(err)

}
