import os
from dotenv import load_dotenv
from google.cloud import storage


def load_env(env_file):
    load_dotenv(env_file)


def setup(env_file="qa.env"):
    load_env(env_file)
    return storage.Client()


def list_buckets(storage_client):
    for bucket in storage_client.list_buckets():
        print(bucket.name)


def main(client):
    bucket_name = "mayankdemobucket"
    list_buckets(client)

if __name__ == "__main__":
    env_file = "qa.env"
    service_key = True
    client = setup(env_file)
    main(client)
