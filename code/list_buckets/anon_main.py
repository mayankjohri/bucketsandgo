import os
from dotenv import load_dotenv
from google.cloud import storage

def get_storage_client():
    client = storage.Client.create_anonymous_client()
    return client

def load_env(env_file):
    load_dotenv(env_file)


def setup():
    return get_storage_client()

def list_buckets(storage_client):

    for bucket in storage_client.list_buckets():
        print(bucket.name)


def main(client):
    # bucket_name = "MayankDemoBucket"
    list_buckets(client)

if __name__ == "__main__":
    # env_file = "qa.env"
    # service_key = True
    # client = setup(env_file, service_key)
    client = setup()
    main(client)
