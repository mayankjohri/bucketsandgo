package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"cloud.google.com/go/storage"
	"github.com/joho/godotenv"
	"google.golang.org/api/iterator"
)

// Function reads
func loadEnv(envFile string) {
	err := godotenv.Load(envFile)
	if err != nil {
		log.Fatalf("Some error occured. Err: %s", err)
	}

	//	os.Getenv("GOOGLE_APPLICATION_CREDENTIALS")
}

// Function to check error, log it and exit.
func LogError(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func NewStorageClient() (*storage.Client, *context.Context, error) {
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		log.Fatal(err)
	}
	return client, &ctx, err
}

func ListBuckets(client *storage.Client, ctx *context.Context, projectID string) {
	fmt.Println("Current Buckets:")
	// ctx1, cancel := context.WithTimeout(*ctx, time.Second*30)
	// defer cancel()

	it := client.Buckets(*ctx, projectID)
	for {
		battrs, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(battrs.Name)
	}
}

func main() {
	loadEnv("qa.env")
	projectID := os.Getenv("PROJECT_ID")

	client, ctx, err := NewStorageClient()
	defer client.Close()
	LogError(err)
	// Add Code below for various operation on buckets
	ListBuckets(client, ctx, projectID)

}
