import os
from dotenv import load_dotenv
from google.cloud import storage

def get_storage_client(service_key=None):

    if service_key:
        storage_client = storage.Client.from_service_account_json(service_key)
    else:
        storage_client = storage.Client()

    return storage_client

def load_env(env_file):
    load_dotenv(env_file)


def setup(env_file="qa.env", service_key=True):
    if service_key:
        load_env(env_file)
    service_key = os.environ.get('TYPE')
    return get_storage_client(service_key)

def list_files(storage_client, bucket_name):
    bucket = storage_client.get_bucket(bucket_name)
    for obj in  storage_client.list_blobs(bucket_name):
        print(obj.name)


def list_files_in_folder(storage_client, bucket_name, prefix="", delimit=""):
    bucket = storage_client.get_bucket(bucket_name)
    for obj in  storage_client.list_blobs(bucket_name, 
            prefix=prefix,
            delimiter=delimit):
        print(obj.name)

def main(client):
    # bucket_name = "MayankDemoBucket"
    bucket_name = "kmsgbzyuug"
    list_files(client, bucket_name)
    print("*"*10)
    list_files_in_folder(client, bucket_name, "test")
    print("*"*10)
    list_files_in_folder(client, bucket_name, "/")

if __name__ == "__main__":
    env_file = "qa.env"
    service_key = True
    client = setup(env_file, service_key)
    main(client)
