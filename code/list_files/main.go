package main

import (
	"context"
	"fmt"
	"log"

	"cloud.google.com/go/storage"
	"github.com/joho/godotenv"
	"google.golang.org/api/iterator"
)

// Function reads
func loadEnv(envFile string) {
	err := godotenv.Load(envFile)
	if err != nil {
		log.Fatalf("Some error occured. Err: %s", err)
	}
}

// Function to check error, log it and exit.
func LogError(err error, message string) {
	if err != nil {
		result := fmt.Sprintf("%s due to error: %s", message, err)
		log.Fatalln(result)
	}
}

func NewStorageClient() (*storage.Client, *context.Context, error) {
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		log.Fatal(err)
	}
	return client, &ctx, err
}

func ListFiles(client *storage.Client, ctx *context.Context,
	bucketName string) error {
	bucket := client.Bucket(bucketName)
	files := bucket.Objects(*ctx, nil)
	// Lets iterate over the bucket files
	for {
		attrs, err := files.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return fmt.Errorf("Bucket(%s).Objects: %v", bucketName, err)
		}
		fmt.Println("File:", attrs.Name)

	}

	return nil
}
func ListFilesInFolder(client *storage.Client, ctx *context.Context,
	bucketName string, folder string) error {
	bucket := client.Bucket(bucketName)
	files := bucket.Objects(*ctx, &storage.Query{
		Prefix: folder,
	})
	// Lets iterate over the bucket files
	for {
		attrs, err := files.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return fmt.Errorf("Bucket(%s).Objects: %v", bucketName, err)
		}
		fmt.Println("File:", attrs.Name)

	}

	return nil
}

func ListFilesInFolderOnly(client *storage.Client, ctx *context.Context,
	bucketName string, folder string) error {
	bucket := client.Bucket(bucketName)
	files := bucket.Objects(*ctx, &storage.Query{
		Prefix:    folder,
		Delimiter: "/",
	})
	// Lets iterate over the bucket files
	for {
		attrs, err := files.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return fmt.Errorf("Bucket(%s).Objects: %v", bucketName, err)
		}
		fmt.Println("File:", attrs.Name)

	}

	return nil
}

func main() {

	loadEnv("qa.env")
	// projectID := os.Getenv("PROJECT_ID")
	bucketName := "kmsgbzyuug"
	client, ctx, err := NewStorageClient()
	defer client.Close()
	LogError(err, "Failed to create Client")
	// Add Code below for various operation on buckets
	ListFiles(client, ctx, bucketName)
	ListFilesInFolder(client, ctx, bucketName, "test/")
	ListFilesInFolderOnly(client, ctx, bucketName, "test/")
}
