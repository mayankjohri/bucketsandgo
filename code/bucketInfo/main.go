package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"cloud.google.com/go/storage"
	"github.com/joho/godotenv"
)

// Function reads
func loadEnv(envFile string) {
	err := godotenv.Load(envFile)
	if err != nil {
		log.Fatalf("Some error occured. Err: %s", err)
	}

	//	os.Getenv("GOOGLE_APPLICATION_CREDENTIALS")
}

// Function to check error, log it and exit.
func LogError(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func NewStorageClient() (*storage.Client, *context.Context, error) {
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		log.Fatal(err)
	}
	return client, &ctx, err
}

func BucketInfo(client *storage.Client, ctx *context.Context, projectID string, bucketName string) error {
	// bucket := client.Bucket(bucketName)
	attrs, err := client.Bucket(bucketName).Attrs(*ctx)
	if err != nil {
		fmt.Println(err)
		return fmt.Errorf("Bucket(%q).Attrs: %v", bucketName, err)
	}
	fmt.Println("BucketName: ", attrs.Name)
	fmt.Println("Location: ", attrs.Location)
	fmt.Println("LocationType: ", attrs.LocationType)
	fmt.Println("StorageClass: ", attrs.StorageClass)
	fmt.Println("Turbo replication (RPO): ", attrs.RPO)
	fmt.Println("TimeCreated: ", attrs.Created)
	fmt.Println("Metageneration: ", attrs.MetaGeneration)
	fmt.Println("PredefinedACL: ", attrs.PredefinedACL)
	return nil
}

func main() {

	loadEnv("qa.env")
	projectID := os.Getenv("PROJECT_ID")
	bucketName := "kmsgbzyuug"
	client, ctx, err := NewStorageClient()
	defer client.Close()
	LogError(err)
	// Add Code below for various operation on buckets
	BucketInfo(client, ctx, projectID, bucketName)

}
