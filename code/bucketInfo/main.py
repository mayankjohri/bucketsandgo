import os
from dotenv import load_dotenv
from google.cloud import storage

def get_storage_client(service_key=None):

    if service_key:
        storage_client = storage.Client.from_service_account_json(service_key)
    else:
        storage_client = storage.Client()

    return storage_client

def load_env(env_file):
    load_dotenv(env_file)


def setup(env_file="qa.env", service_key=True):
    if service_key:
        load_env(env_file)
    service_key = os.environ.get('TYPE')
    return get_storage_client(service_key)

def bucket_info(storage_client, bucket_name):
    try:
        bucket = storage_client.get_bucket(bucket_name)
    except Exception as e:
        print(f"Sorry not able to find the bucket: {bucket_name}, Err: {e}")
        return
    # bucket = storage_client.lookup_bucket(bucket_name)
    if bucket:
        print(f"ID: {bucket.id}")
        print(f"Name: {bucket.name}")
        print(f"Storage Class: {bucket.storage_class}")
        print(f"Cors: {bucket.cors}")
        print(f"Labels: {bucket.labels}")
        print(f"Location: {bucket.location}")
        print(f"Location Type: {bucket.location_type}")
        print(f"Metageneration: {bucket.metageneration}")
        print(f"Default KMS Key Name: {bucket.default_kms_key_name}")
        print(f"Retention Period: {bucket.retention_period}")
        print(f"Retention Policy Locked: {bucket.retention_policy_locked}")
        print(f"Retention Effective Time: {bucket.retention_policy_effective_time}")
        print(f"Self Link: {bucket.self_link}")
        print(f"Time Created: {bucket.time_created}")
        print(f"Requester Pays: {bucket.requester_pays}")
        print(f"Versioning Enabled: {bucket.versioning_enabled}")
        print(f"Default Event Based Hold: {bucket.default_event_based_hold}")
        print(f"Public Access Prevention:"
                " {bucket.iam_configuration.public_access_prevention}")
    else:
        print("Sorry not able to find the bucket")

def main(client):
    # bucket_name = "MayankDemoBucket"
    bucket_name = "kmsgbzyuug"
    # bucket_name = "kmsgbzyuug123"
    bucket_info(client, bucket_name)

if __name__ == "__main__":
    env_file = "qa.env"
    service_key = True
    client = setup(env_file, service_key)
    main(client)
