package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"

	"cloud.google.com/go/storage"
	"github.com/joho/godotenv"
)

// Function reads
func loadEnv(envFile string) {
	err := godotenv.Load(envFile)
	if err != nil {
		log.Fatalf("Some error occured. Err: %s", err)
	}
}

// Function to check error, log it and exit.
func LogError(err error, message string) {
	if err != nil {
		result := fmt.Sprintf("%s due to error: %s", message, err)
		log.Fatalln(result)
	}
}

func NewStorageClient() (*storage.Client, *context.Context, error) {
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		log.Fatal(err)
	}
	return client, &ctx, err
}

func DownloadFile(client *storage.Client, ctx *context.Context,
	bucketName string, srcFileName string,
	dstFileName string) error {
	bucket := client.Bucket(bucketName)

	dfp, err := os.Create(dstFileName)
	if err != nil {
		return fmt.Errorf("os.Create: %v", err)
	}

	rc, err := bucket.Object(srcFileName).NewReader(*ctx)
	if err != nil {
		LogError(err, "Sorry, I failed to open file for download")
	}
	defer rc.Close()

	if _, err := io.Copy(dfp, rc); err != nil {
		return fmt.Errorf("io.Copy: %v", err)
	}

	if err = dfp.Close(); err != nil {
		return fmt.Errorf("f.Close: %v", err)
	}

	return nil
}

func main() {

	loadEnv("qa.env")
	// projectID := os.Getenv("PROJECT_ID")
	bucketName := "kmsgbzyuug"
	client, ctx, err := NewStorageClient()
	defer client.Close()
	LogError(err, "Failed to create Client")
	// Add Code below for various operation on buckets
	DownloadFile(client, ctx, bucketName, "test/alpine-extended-3.15.1-x86_64.iso", "alpine-extended-3.15.1-x86_64.iso")
}
