import os
from dotenv import load_dotenv
from google.cloud import storage

def get_storage_client(service_key=None):

    if service_key:
        storage_client = storage.Client.from_service_account_json(service_key)
    else:
        storage_client = storage.Client()

    return storage_client

def load_env(env_file):
    load_dotenv(env_file)


def setup(env_file="qa.env", service_key=True):
    if service_key:
        load_env(env_file)
    service_key = os.environ.get('TYPE')
    return get_storage_client(service_key)

def display_file(file_name):
    with open(file_name) as fp:
        print(fp.read())

def download_file(storage_client, bucket_name, obj_name, dst_file):
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(obj_name)
    blob.download_to_filename(dst_file)

def main(client):
    # bucket_name = "MayankDemoBucket"
    bucket_name = "kmsgbzyuug"
    dst_file = "test.txt"
    download_file(client, bucket_name, "test/copyfile.py", dst_file)
    display_file(dst_file)

if __name__ == "__main__":
    env_file = "qa.env"
    service_key = True
    client = setup(env_file, service_key)
    main(client)
