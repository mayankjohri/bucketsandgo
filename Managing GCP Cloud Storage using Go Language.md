# Lets Learn GCP Part 1: Managing GCP Cloud Storage

In todays world, almost everything is going migrating towards cloud in some or other variation, Thus it makes more sense to start migrating vital project/personal data on the cloud. 

The main reason for migration to Cloud storage is

-  automated backup's, 

- redundancy, 

- data recovery and 

- helps in implementing disaster recovery policies. 

One of the most commonly used Cloud service provider is Google through Google Cloud Platform (**GCP**). It provides multiple options to store files which are 

- [Google Drive](https://www.google.com/intl/en/drive/): Mostly used to store Personal/Individual users files not ideal for shared environment.
- [Cloud Storage for Firebase](https://firebase.google.com/docs/storage/): Mostly used by mobile/Web applications to store user/app data.
- [Filestore](https://cloud.google.com/filestore/docs): Another offering from firebase to implement file-based workload.
- [Persistent Disk](https://cloud.google.com/compute/docs/disks): Mostly used for disks of Compute Engine virtual machine.
- Cloud Storage or Buckets: Mostly used to store project/User data for long term storage.

In this book, we will cover buckets aka Cloud Storage as they are the most convenient way to manage project/App date.

## Bucket / Cloud Storage
Buckets are part of Cloud Storage offering from GCP. They are container's which are used to store files and folder. We can think of buckets as disk drivers and as in local OS all files are stored inside a disk. In Cloud Storage buckets serve that purpose. We can define which region the bucket and files it store, should be located. 

Other key features are

- Keys & values can each have max size of 63 characters.
- Only lowercase letters,  unicode alphabets, numeric characters, underscores, and dashes are allowed for Keys and values.
- Starting character of keys  must be a lowercase letter including international characters.
- Empty keys are not allowed.
- bucket labels are not associated with individual objects or object metadata

>  **Note**:

> - In cloud storage, files needs to be inside a bucket.
>
> - The buckets are unique and No two buckets can have same name.
> - GCP allows up-to 64 labels per bucket.

## Available storage classes

The following table summarizes the primary storage classes offered by Cloud Storage. See class descriptions for a complete discussion.


| Storage Class | Name for APIs and gsutil |	Minimum storage duration | Typical monthly availability |
|-------|-------------|-----------------|--------|
| Standard Storage 	| STANDARD 	| None 	| - `>99.99%` in multi-regions and dual-regions <br/>- 99.99% in regions |
| Nearline Storage 	| NEARLINE 	| 30 days | - 99.95% in multi-regions and dual-regions<br/>- 99.9% in regions |
| Coldline Storage | COLDLINE | 90 days | - 99.95% in multi-regions and dual-regions<br/>- 99.9% in regions |
|Archive Storage | ARCHIVE 	| 365 days 	| - 99.95% in multi-regions and dual-regions<br/>- 99.9% in regions |

## Bucket locations

When creating the bucket, we can define the physical location(s) of them. `Region` is a specific` physical location such where Google Servers are located.

Its good idea to have bucket location near your clients or your consumption point such as App Servers.  

Following options are available for bucket locations.

| Location       | Meaning                                                      |
| -------------- | ------------------------------------------------------------ |
| *region*       | Bucket and all its content are stored in single location. Normally in production system this is to be avoided as it introduces single point of failure. |
| *dual-region*  | The bucket and its contents will be located in two regions specified while creating the buckets. |
| *multi-region* | The bucket and its contents will be located in regions specified while creating the buckets. |

### Regions

All regions are at least 100 miles apart.

| Continent         | Region Name               | Region Description |                                                              |
| ----------------- | ------------------------- | ------------------ | ------------------------------------------------------------ |
| **North America** |                           |                    |                                                              |
|                   | `NORTHAMERICA-NORTHEAST1` | Montréal           | ![leaf icon](Managing GCP Cloud Storage using Go Language.assets/gleaf.svg) [Low CO2](https://cloud.google.com/sustainability/region-carbon) |
|                   | `NORTHAMERICA-NORTHEAST2` | Toronto            | ![leaf icon](Managing GCP Cloud Storage using Go Language.assets/gleaf.svg) [Low CO2](https://cloud.google.com/sustainability/region-carbon) |
|                   | `US-CENTRAL1`             | Iowa               | ![leaf icon](Managing GCP Cloud Storage using Go Language.assets/gleaf.svg) [Low CO2](https://cloud.google.com/sustainability/region-carbon) |
|                   | `US-EAST1`                | South Carolina     |                                                              |
|                   | `US-EAST4`                | Northern Virginia  |                                                              |
|                   | `US-WEST1`                | Oregon             | ![leaf icon](Managing GCP Cloud Storage using Go Language.assets/gleaf.svg) [Low CO2](https://cloud.google.com/sustainability/region-carbon) |
|                   | `US-WEST2`                | Los Angeles        |                                                              |
|                   | `US-WEST3`                | Salt Lake City     |                                                              |
|                   | `US-WEST4`                | Las Vegas          |                                                              |
| **South America** |                           |                    |                                                              |
|                   | `SOUTHAMERICA-EAST1`      | São Paulo          | ![leaf icon](Managing GCP Cloud Storage using Go Language.assets/gleaf.svg) [Low CO2](https://cloud.google.com/sustainability/region-carbon) |
|                   | `SOUTHAMERICA-WEST1`      | Santiago           |                                                              |
| **Europe**        |                           |                    |                                                              |
|                   | `EUROPE-CENTRAL2`         | Warsaw             |                                                              |
|                   | `EUROPE-NORTH1`           | Finland            | ![leaf icon](Managing GCP Cloud Storage using Go Language.assets/gleaf.svg) [Low CO2](https://cloud.google.com/sustainability/region-carbon) |
|                   | `EUROPE-WEST1`            | Belgium            | ![leaf icon](Managing GCP Cloud Storage using Go Language.assets/gleaf.svg) [Low CO2](https://cloud.google.com/sustainability/region-carbon) |
|                   | `EUROPE-WEST2`            | London             |                                                              |
|                   | `EUROPE-WEST3`            | Frankfurt          |                                                              |
|                   | `EUROPE-WEST4`            | Netherlands        |                                                              |
|                   | `EUROPE-WEST6`            | Zürich             | ![leaf icon](Managing GCP Cloud Storage using Go Language.assets/gleaf.svg) [Low CO2](https://cloud.google.com/sustainability/region-carbon) |
| **Asia**          |                           |                    |                                                              |
|                   | `ASIA-EAST1`              | Taiwan             |                                                              |
|                   | `ASIA-EAST2`              | Hong Kong          |                                                              |
|                   | `ASIA-NORTHEAST1`         | Tokyo              |                                                              |
|                   | `ASIA-NORTHEAST2`         | Osaka              |                                                              |
|                   | `ASIA-NORTHEAST3`         | Seoul              |                                                              |
|                   | `ASIA-SOUTH1`             | Mumbai             |                                                              |
|                   | `ASIA-SOUTH2`             | Delhi              |                                                              |
|                   | `ASIA-SOUTHEAST1`         | Singapore          |                                                              |
|                   | `ASIA-SOUTHEAST2`         | Jakarta            |                                                              |
| **Australia**     |                           |                    |                                                              |
|                   | `AUSTRALIA-SOUTHEAST1`    | Sydney             |                                                              |
|                   | `AUSTRALIA-SOUTHEAST2`    | Melbourne          |                                                              |

### Multi-regions

| Multi-Region Name | Multi-Region Description                                     |
| ----------------- | ------------------------------------------------------------ |
| `ASIA`            | Data centers in Asia                                         |
| `EU`              | Data centers within [member states](https://europa.eu/european-union/about-eu/countries_en) of the European Union<sup>1</sup> |
| `US`              | Data centers in the United States                            |

<sup>1</sup> Object data added to a bucket in the `EU` multi-region is not stored in the `EUROPE-WEST2` (London) or `EUROPE-WEST6` (Zurich) regions.

### Dual-regions

| Dual-Region Name | Dual-Region Description                  |
| ---------------- | ---------------------------------------- |
| `ASIA1`          | `ASIA-NORTHEAST1` and `ASIA-NORTHEAST2`. |
| `EUR4`           | `EUROPE-NORTH1` and `EUROPE-WEST4`.      |
| `NAM4`           | `US-CENTRAL1` and `US-EAST1`.            |

### Pricing

Pricing details can be found at https://cloud.google.com/storage/pricing

## Terminology
Lets few few of the new terminology keywords which will be used in this document. 

### Bucket label

They are `key:value` pairs of meta-data which can be used to store meta-data about the files and bucket.

### Objects

Buckets stores files as part of objects. Objects are unique entities consisting of immutable file as object data and meta-data as object meta-data. 

#### Object names

An object name is part of object meta-data which can have any name provided its smaller then 1024 in bytes length. They can have unicode values as well. 

Internally, GCP handle all the objects independently with no relationship with its peers, but still provides a user view, where they can be viewed in groups, similar to *inx file system.

#### Object immutability

Once an object is uploaded on the server, it cannot be changed ever. We can delete it, upload newer version, but never update it. 

### Resources
It's an entity within Google Cloud. Everything is a resource such as project, bucket, and object in Google Cloud.

### Egress 
It is the data which is sent by Cloud Storage in HTTP Response

### Ingress
It is the data which is sent to Cloud Storage in HTTP Response

#### Bucket Name rules

Before we can create a bucket, we need to remember that there are few limitations/guidelines imposed by Google regarding the bucket name such as :

- They can only contain `lowercase`, numeric, dashes (-), underscores (_), and dots (.) characters.f
- **Should** can not have ip address as bucket name, for example, `191.121.1.11` or `10.0.0.1` etc, unless you own that IP Address.
- They can not start with  "goog" or some similar variation such as `g00gle` or `g00g1e`

## Setup

Before we can begin using buckets through Go Language, we need to do setup proper authorization and authentication. We will discuss about them in this section.

### Required Permissions

We will need different types of permissions for various operations, as shown in the table below

| Action | Object ACL permission | Equivalent IAM role |
|---------|-----------------------|--------------------|
| Create Bucket |      |                               |
| Delete Bucket |       |                             |
| List Buckets | READER | Storage Legacy Object Reader (`roles/storage.legacyObjectReader`) |
| Download Files |       |                             |
| Uploading Files |      |                             |
| All | OWNER | Storage Legacy Object Owner (`roles/storage.legacyObjectOwner`) |

### Service Key

GCP provides many methods to authenticate, one of the most common and easy is using service keys. It can be created in GCP IAM module.

## Coding

### Boilerplate Code

Lets create the basic code upon which we will be using in the entire document. This will include modules such as authentication etc. 

```go
package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"cloud.google.com/go/storage"
	"github.com/joho/godotenv"
	"google.golang.org/api/option"
)

// 
// Function reads 
func loadEnv(envFile string){
	err := godotenv.Load(envFile)
	if err != nil {
		log.Fatalf("Some error occured. Err: %s", err)
	}

	val := os.Getenv("GOOGLE_APPLICATION_CREDENTIALS")
	return val
}

// Function to check error, log it and exit.
func LogError(err error) {
	if err != nil {
	        log.Fatalln(err)
	}
}

// Function to use
func explicitUsingJSONKeys(jsonPath string) (*storage.Client,
	*context.Context,
	error) {
	ctx := context.Background()
	client, err := storage.NewClient(ctx, option.WithCredentialsFile(jsonPath))
	LogError(err)
	return client, &ctx, err
}

func main() {
	projectID := os.Getenv("PROJECT_ID")
	fileLoc := loadEnv()
	client, ctx, err := explicitUsingJSONKeys(fileLoc)
	defer client.Close()
	LogError(err)

	// Add Code below for various operation on buckets
	fmt.Println(ctx)
}
```

 Please note, if we are populating `GOOGLE_APPLICATION_CREDENTIALS` environment variable, then we don't need the JSON file path and default client should do the trick as shown in below code

```go
func NewStorageClient() (*storage.Client, *context.Context, error) {
    ctx := context.Background()
    client, err := storage.NewClient(ctx)
    if err != nil {
        log.Fatal(err)
    }
    return client, &ctx, err
}
```

Also,  to access public data, an unauthenticated client can be created using the following code instead.

```go
client, err := storage.NewClient(ctx, option.WithoutAuthentication())
```

#### Timeouts 

By default, client has a default timeout for non-streaming API calls such as `Create` & `GET`, but it might  not suitable for every use-case, thus we can provide our custom timeout as shown in the below code

```go
// Time out set for 15 seconds
tctx, cancel := context.WithTimeout(ctx, 15*time.Second)
defer cancel() // Never miss the cancel.
```

#### Cancel

We can also cancel any running request by exposing the `cancel` function, as shown in the below code

```go
cctx, cancel := context.WithCancel(ctx)
defer cancel() // Never miss the cancel.
```

Now, while we are making a long running API request, `cancel` function can be called from another thread to cancel the API request, namely from GUI `cancel` button.

>  **Note**: Get bucket information
>
> <hr>
>
>  To create a client we do not need the project_id. 

### Creating Buckets

```go
package main

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"

	"cloud.google.com/go/storage"
	"github.com/joho/godotenv"
)

//Common Functions

const charset = "abcdefghijklmnopqrstuvwxyz"

var seededRand *rand.Rand = rand.New(
	rand.NewSource(time.Now().UnixNano()))

func RandomString(length int, charset string) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

// Function reads
func loadEnv(envFile string) {
	err := godotenv.Load(envFile)
	if err != nil {
		log.Fatalf("Some error occured. Err: %s", err)
	}

	os.Getenv("GOOGLE_APPLICATION_CREDENTIALS")
}

// Function to check error, log it and exit.
func LogError(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func NewStorageClient() (*storage.Client, *context.Context, error) {
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		log.Fatal(err)
	}
	return client, &ctx, err
}

func CreateBucket(projectID, bucketName string) error {
	fmt.Println("Starting the process to create the bucket")
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		return fmt.Errorf("Sorry, failed to create Storage Client: %v", err)
	}
	defer client.Close()

	storageClassAndLocation := &storage.BucketAttrs{
		StorageClass: "COLDLINE",
		Location:     "asia",
	}
	bucket := client.Bucket(bucketName)
	if err := bucket.Create(ctx, projectID, storageClassAndLocation); err != nil {
		return fmt.Errorf("Bucket(%q).Create: %v", bucketName, err)
	}
	fmt.Println("Created bucket %v in %v with storage class %v\n",
		bucketName,
		storageClassAndLocation.Location,
		storageClassAndLocation.StorageClass)
	return nil
}

func main() {
	loadEnv("qa.env")
	projectID := os.Getenv("PROJECT_ID")
	client, ctx, err := NewStorageClient()
	defer client.Close()
	LogError(err)

	// Add Code below for various operation on buckets
	fmt.Println(ctx)
	bucketName := RandomString(10, charset)
	err = CreateBucket(projectID, bucketName)
	if err != nil {
		fmt.Println(err)
	}
}
```

### Listing Buckets

`client.Buckets` returns the collection with all the buckets details present in the project `projectID`

````go
func ListBuckets(client *storage.Client, ctx *context.Context, projectID string) {
	fmt.Println("Buckets present:")
	buckets := client.Buckets(*ctx, projectID)
	for {
		bucketAttrs, err := buckets.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(bucketAttrs.Name)
	}
}
````

### Deleting Buckets

Once we delete a bucket,  all the objects stored in the bucket are also deleted and cannot be recovered. Thus delete the bucket with caution. 

```go
func DeleteBuckets(client *storage.Client, 
                   ctx *context.Context, 
                   projectID string, 
                   bucketName string) error {
 	bucket := client.Bucket(bucketName)
	if err := bucket.Delete(*ctx); err != nil {
		return fmt.Errorf("Bucket(%q).Delete: %v", bucketName, err)
	}
	return nil
}
```

### Move and rename buckets

GCP API's do not directly support move or rename, we need to create a new bucket, then copy all object from the original bucket to new bucket and then delete older bucket.

### Get bucket information

Bucket information is stored in its metadata which can be read using the `Attrs(ctx)` function as shown in the below code.

```go
func BucketInfo(client *storage.Client, ctx *context.Context, projectID string, bucketName string) error {
	attrs, err := client.Bucket(bucketName).Attrs(*ctx)
	if err != nil {
		return fmt.Errorf("Bucket(%q).Attrs: %v", bucketName, err)
	}
    
	fmt.Println("BucketName: ", attrs.Name)
	fmt.Println("Location: ", attrs.Location)
	fmt.Println("TimeCreated: ", attrs.Created)
	fmt.Println("LocationType: ", attrs.LocationType)
	fmt.Println("StorageClass: ", attrs.StorageClass)
	fmt.Println("PredefinedACL: ", attrs.PredefinedACL)
	fmt.Println("Turbo replication (RPO): ", attrs.RPO)
	fmt.Println("Metageneration: ", attrs.MetaGeneration)
	return nil
}
```

Output:

```go
BucketName:  kmaewrasrtrtyug
Location:  ASIA
LocationType:  multi-region
StorageClass:  COLDLINE
Turbo replication (RPO):  DEFAULT
TimeCreated:  2022-03-08 02:41:44.132 +0000 UTC
Metageneration:  1
PredefinedACL:  
```

### Upload File

Uploading any file to bucket is a a multi step process using `Go` language, we need to 

- read the file in memory, 
- create a bucket with destination file name
- Copy file data to bucket as shown in below code

```go
func CopyFile(client *storage.Client, ctx *context.Context,  
    bucketName string, srcFileName string,  
    dstFileName string) error {  
    bucket := client.Bucket(bucketName)  
    fp, err := os.Open(srcFileName)  
    if err != nil {    
        LogError(err,  
            "Sorry, I am not able to read data from file "+srcFileName)  
    }  
    defer fp.Close()  
    obj := bucket.Object(dstFileName)  
    wc := obj.NewWriter(*ctx)  
    if _, err = io.Copy(wc, fp); err != nil {  
        return fmt.Errorf("io.Copy: %v", err)  
    }  
    if err = wc.Close(); err != nil {  
        fmt.Println("Error", err)
        return fmt.Errorf("Writer.Close: %v", err)
    }
    return nil
}

```
### Listing Files

We can list all files in the bucket using the following code

```go
func ListFiles(client *storage.Client, ctx *context.Context,
	bucketName string) error {
	bucket := client.Bucket(bucketName)
	files := bucket.Objects(*ctx, nil)
	// Lets iterate over the bucket files
	for {
		attrs, err := files.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return fmt.Errorf("Bucket(%s).Objects: %v", bucketName, err)
		}
		fmt.Println("File:", attrs.Name)

	}

	return nil
}
```

but most of the time, we don't want all the files and want to view files from a particular folder and its sub folder, in that case we can use `prefix` to denote the folder. 

```go
func ListFilesInFolder(client *storage.Client, ctx *context.Context,
	bucketName string, folder string) error {
	bucket := client.Bucket(bucketName)
	files := bucket.Objects(*ctx, &storage.Query{
		Prefix: folder,
	})
	// Lets iterate over the bucket files
	for {
		attrs, err := files.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return fmt.Errorf("Bucket(%s).Objects: %v", bucketName, err)
		}
		fmt.Println("File:", attrs.Name)

	}

	return nil
}
```

We can also restrict the list of files to folder only excluding its sub folder by setting `Delimiter` value to `/` as shown in below code

```go
func ListFilesInFolderOnly(client *storage.Client, ctx *context.Context,
	bucketName string, folder string) error {
	bucket := client.Bucket(bucketName)
	files := bucket.Objects(*ctx, &storage.Query{
		Prefix:    folder,
		Delimiter: "/",
	})
	// Lets iterate over the bucket files
	for {
		attrs, err := files.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return fmt.Errorf("Bucket(%s).Objects: %v", bucketName, err)
		}
		fmt.Println("File:", attrs.Name)

	}

	return nil
}
```

### Download File

We can download file using the technique shown in the following code sample

```go
func DownloadFile(client *storage.Client, ctx *context.Context,
	bucketName string, srcFileName string,
	dstFileName string) error {
	bucket := client.Bucket(bucketName)

	dfp, err := os.Create(dstFileName)
	if err != nil {
		return fmt.Errorf("os.Create: %v", err)
	}

	rc, err := bucket.Object(srcFileName).NewReader(*ctx)
	if err != nil {
		LogError(err, "Sorry, I failed to open file for download")
	}
	defer rc.Close()

	if _, err := io.Copy(dfp, rc); err != nil {
		return fmt.Errorf("io.Copy: %v", err)
	}

	if err = dfp.Close(); err != nil {
		return fmt.Errorf("f.Close: %v", err)
	}

	return nil
}
```

