# ![Title_Managing GCP Cloud Storage Using Python](Managing GCP Cloud Storage using Python Language.assets/Title_Managing GCP Cloud Storage Using Python.jpg)

# Part 1: Managing GCP Cloud Storage [alpha-1]

[toc]

Today almost everything is migrating towards cloud in some or other variation. Which has let to multiple vendors providing their own flavor of cloud offering. 

Google is one of the major company which are providing their own flavor of Cloud offering called **Google Cloud Platform** (_GCP_). One of the major user requirement is storing their data (public/private data) for their project. 

The major reasons for client to migrate to Cloud storage are

-  automated backup's, 
-  data recovery,
-  data redundancy and, 
-   facilitates in implementing disaster recovery policies by providing multiple options. 

_GCP_ provides following options to store files or files based data.

- [Google Drive](https://www.google.com/intl/en/drive/): Mostly used to store Personal/Individual users files not ideal for shared environment.
- [Cloud Storage for Firebase](https://firebase.google.com/docs/storage/): Mostly used by mobile/Web applications to store user/app data.
- [Filestore](https://cloud.google.com/filestore/docs): Another offering from firebase to implement file-based workload.
- [Persistent Disk](https://cloud.google.com/compute/docs/disks): Mostly used for disks of Compute Engine virtual machine.
- Cloud Storage or Buckets: Mostly used to store project/User data for long term storage.

In this book, we will cover _Cloud Storage_ aka buckets. They are the most convenient way to manage project/App persistent data.

## Bucket / Cloud Storage
Buckets can be thought of as containers which are used to store files and folder. We can also think buckets as disk drivers/disk partition and as in local system we use partition/partition to store files inside a disk, in Cloud Storage, buckets are used to store the files & organized using various folder. 

Buckets provide additional features, where we can define how & where the data is stored. Should they be in single region or multi-region, for how long the data should be persistent and can even define its life-cycle. 

Buckets can have labels, using which custom meta-data can be stored by the programmer as key/value pair. Few things to remember about them is: 

- Only lowercase letters,  unicode alphabets, numeric characters, underscores, and dashes are allowed for Keys and values.
- Keys & values can each have max size of 63 characters.
- Empty label is not allowed. i.e. the key part of labels can not be blank/just space 
- Starting character of keys must be a lowercase letter including international characters.
- bucket labels cannot be associated with individual objects or object meta-data

>  **Note**:

> - In cloud storage, files needs to be inside a bucket.
>
> - No two buckets can have same name.
> - GCP allows up-to 64 labels per bucket.

## Available storage classes

_GCP_ provides multiple storage classes to handle various client requirements depending on how persistent data they want to store.

### Standard Storage

Standard Storage class can be used for the data which is frequently used and/or needed only for small duration and can be deleted afterwards.

 ### Nearline Storage

It can be used for data which are not frequently used, such as once a month or less. Using this data class is bit cheaper then standard storage for storage, but should not be frequently used.  

### Coldline Storage

It is again cheaper to store data in coldline storage, if we are not very frequently using it such as once per quarter. 

### Archive Storage

It is the cheapest to store data in `archive storage`, if the data is accessed few times a year. It is best for archiving the data for long term. 

### Summary

The details are summarized in the following table.


| Storage Class | API Argument |	Minimum storage duration | Typical monthly availability |
|-------|-------------|-----------------|--------|
| Standard Storage 	| STANDARD 	| None 	| - `>99.99%` in multi-regions and dual-regions <br/>- 99.99% in regions |
| Nearline Storage 	| NEARLINE 	| 30 days | - 99.95% in multi-regions and dual-regions<br/>- 99.9% in regions |
| Coldline Storage | COLDLINE | 90 days | - 99.95% in multi-regions and dual-regions<br/>- 99.9% in regions |
|Archive Storage | ARCHIVE 	| 365 days 	| - 99.95% in multi-regions and dual-regions<br/>- 99.9% in regions |

## Bucket locations

Bucket location can be defined at the time of its creation using `region`. A region is a specific geographical location where you can host your resources.  

Its usually a good idea for bucket be located near its consumption point such as the clients, Web Servers, App Servers etc.

Options are available for bucket locations are as follows.

| Location       | Meaning                                                      |
| -------------- | ------------------------------------------------------------ |
| *region*       | Bucket and all its content are stored in single location. NEVER use it in production enviornment, as it introduces single point of failure |
| *dual-region*  | The bucket and its contents will be located in two specified regions. |
| *multi-region* | The bucket and its contents will be located in multi specified regions. |

### Regions

In GCP, all regions are at least 160 Kms apart [As on (Apr 2022)], The regions might change with time.

| Continent         | Region Name               | Region Description |
| ----------------- | ------------------------- | ------------------ |
| **North America** |                           |                    |
|                   | `NORTHAMERICA-NORTHEAST1` | Montréal           |
|                   | `NORTHAMERICA-NORTHEAST2` | Toronto            |
|                   | `US-CENTRAL1`             | Iowa               |
|                   | `US-EAST1`                | South Carolina     |
|                   | `US-EAST4`                | Northern Virginia  |
|                   | `US-WEST1`                | Oregon             |
|                   | `US-WEST2`                | Los Angeles        |
|                   | `US-WEST3`                | Salt Lake City     |
|                   | `US-WEST4`                | Las Vegas          |
| **South America** |                           |                    |
|                   | `SOUTHAMERICA-EAST1`      | São Paulo          |
|                   | `SOUTHAMERICA-WEST1`      | Santiago           |
| **Europe**        |                           |                    |
|                   | `EUROPE-CENTRAL2`         | Warsaw             |
|                   | `EUROPE-NORTH1`           | Finland            |
|                   | `EUROPE-WEST1`            | Belgium            |
|                   | `EUROPE-WEST2`            | London             |
|                   | `EUROPE-WEST3`            | Frankfurt          |
|                   | `EUROPE-WEST4`            | Netherlands        |
|                   | `EUROPE-WEST6`            | Zürich             |
| **Asia**          |                           |                    |
|                   | `ASIA-EAST1`              | Taiwan             |
|                   | `ASIA-EAST2`              | Hong Kong          |
|                   | `ASIA-NORTHEAST1`         | Tokyo              |
|                   | `ASIA-NORTHEAST2`         | Osaka              |
|                   | `ASIA-NORTHEAST3`         | Seoul              |
|                   | `ASIA-SOUTH1`             | Mumbai             |
|                   | `ASIA-SOUTH2`             | Delhi              |
|                   | `ASIA-SOUTHEAST1`         | Singapore          |
|                   | `ASIA-SOUTHEAST2`         | Jakarta            |
| **Australia**     |                           |                    |
|                   | `AUSTRALIA-SOUTHEAST1`    | Sydney             |
|                   | `AUSTRALIA-SOUTHEAST2`    | Melbourne          |

#### Multi-regions

| Multi-Region Name | Multi-Region Description                                     |
| ----------------- | ------------------------------------------------------------ |
| `ASIA`            | Data centers in Asia                                         |
| `EU`              | Data centers within [member states](https://europa.eu/european-union/about-eu/countries_en) of the European Union<sup>1</sup> |
| `US`              | Data centers in the United States                            |

<sup>1</sup> Object data added to a bucket in the `EU` multi-region is not stored in the `EUROPE-WEST2` (London) or `EUROPE-WEST6` (Zurich) regions.

#### Dual-regions

| Dual-Region Name | Dual-Region Description                  |
| ---------------- | ---------------------------------------- |
| `ASIA1`          | `ASIA-NORTHEAST1` and `ASIA-NORTHEAST2`. |
| `EUR4`           | `EUROPE-NORTH1` and `EUROPE-WEST4`.      |
| `NAM4`           | `US-CENTRAL1` and `US-EAST1`.            |

### Pricing

Pricing details can be found at https://cloud.google.com/storage/pricing

## Terminology
Lets few few of the new terminology keywords which will be used in this document

### Bucket label

They are `key:value` pairs of meta-data which can be used to store meta-data about the files and bucket

### Objects

Buckets stores files as part of objects. Objects are unique entities consisting of immutable file as object data and meta-data as object meta-data

#### Object names

An object name is part of object meta-data which can have any name provided its smaller then 1024 in bytes length. They can have unicode values as well

Internally, GCP handle all the objects independently with no relationship with its peers, but still provides a user view, where they can be viewed in groups, similar to *inx file system

#### Object immutability

Once an object is uploaded on the server, it cannot be changed ever. We can delete it, upload newer version, but never update it

### Resources
It's an entity within Google Cloud. Everything is a resource such as project, bucket, and object in Google Cloud.

### Egress 
It is the data which is sent by Cloud Storage in HTTP Response

### Ingress
It is the data which is sent to Cloud Storage in HTTP Response

#### Bucket Name rules

Before we can create a bucket, we need to remember that there are few limitations/guidelines imposed by Google regarding the bucket name such as :

- They can only contain `lowercase`, numeric, dashes (-), underscores (_), and dots (.) characters.
- **Should** can not have ip address as bucket name, for example, `191.121.1.11` or `10.0.0.1` etc, unless you own that IP Address.
- They can not start with  `goog` or some similar variation such as `g00gle`, `g0og1e` or `g00g1e`

## Setup

Before we can begin using buckets through Python Language, we need to do setup proper authorization and authentication. We will discuss about them in this section.

### Required Permissions

We will need different types of permissions for various operations, as shown in the table below

| Bucket permission name         | Description                                                  |
| ------------------------------ | ------------------------------------------------------------ |
| `storage.buckets.create`       | Create new buckets in a project.                             |
| `storage.buckets.delete`       | Delete buckets.                                              |
| `storage.buckets.get`          | Read bucket metadata, excluding IAM policies, and list or read the Pub/Sub notification configurations on a bucket. |
| `storage.buckets.getIamPolicy` | Read bucket IAM policies.                                    |
| `storage.buckets.list`         | List buckets in a project. Also read bucket metadata, excluding IAM policies, when listing. |
| `storage.buckets.setIamPolicy` | Update bucket IAM policies.                                  |
| `storage.buckets.update`       | Update bucket metadata, excluding IAM policies, and add or remove a Pub/Sub notification configuration on a bucket. |

### Object permissions

| Object permission name         | Description                                                  |
| ------------------------------ | ------------------------------------------------------------ |
| `storage.objects.create`       | Add new objects to a bucket.                                 |
| `storage.objects.delete`       | Delete objects.                                              |
| `storage.objects.get`          | Read object data and metadata, excluding ACLs.               |
| `storage.objects.getIamPolicy` | Read object ACLs, returned as IAM policies.                  |
| `storage.objects.list`         | List objects in a bucket. Also read object metadata, excluding ACLs, when listing. |
| `storage.objects.setIamPolicy` | Update object ACLs.                                          |
| `storage.objects.update`       | Update object metadata, excluding ACLs.                      |

> **Note**: 
>
> - The `storage.objects.getIamPolicy` and `storage.objects.setIamPolicy` permissions do not apply to buckets with [uniform bucket-level access](https://cloud.google.com/storage/docs/uniform-bucket-level-access) enabled.
> - In order to replace existing objects, both storage.objects.create and storage.objects.delete permissions are required.

## Multipart upload permissions

| Multipart upload permission name     | Description                                                  |
| ------------------------------------ | ------------------------------------------------------------ |
| `storage.multipartUploads.create`    | Upload objects in multiple parts.                            |
| `storage.multipartUploads.abort`     | Abort multipart upload sessions.                             |
| `storage.multipartUploads.listParts` | List the uploaded object parts in a multipart upload session. |
| `storage.multipartUploads.list`      | List the multipart upload sessions in a bucket.              |

### IAM roles for Cloud Storage

| Role                                                         | Description                                                  | Permissions                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **Storage Object Creator**        (`roles/storage.objectCreator`) | Allows users to create objects. Does not give permission        to view, delete, or replace objects. | `orgpolicy.policy.get`1         `resourcemanager.projects.get`         `resourcemanager.projects.list`         `storage.objects.create`         `storage.multipartUploads.create`         `storage.multipartUploads.abort`         `storage.multipartUploads.listParts` |
| **Storage Object Viewer**        (`roles/storage.objectViewer`) | Grants access to view objects and their metadata,        excluding ACLs.Can also list the objects in a bucket. | `resourcemanager.projects.get`         `resourcemanager.projects.list`         `storage.objects.get`         `storage.objects.list` |
| **Storage Object Admin**        (`roles/storage.objectAdmin`) | Grants full control over objects, including listing,      creating, viewing, and deleting objects. | `orgpolicy.policy.get`1         `resourcemanager.projects.get`         `resourcemanager.projects.list`         `storage.objects.*`         `storage.multipartUploads.*` |
| **Storage HMAC Key Admin**        (`roles/storage.hmacKeyAdmin`) | Full control over HMAC keys in a project. This role can only be        applied to a project. | `orgpolicy.policy.get`1         `storage.hmacKeys.*`         |
| **Storage Admin** (`roles/storage.admin`)                    | Grants full control of buckets and objects. When      applied to an individual **bucket**, control applies only to the      specified bucket and objects within the bucket. | `firebase.projects.get`         `orgpolicy.policy.get`1         `resourcemanager.projects.get`         `resourcemanager.projects.list`         `storage.buckets.*`         `storage.objects.*`         `storage.multipartUploads.*` |

#### Basic Roles

| Role                        | Description                                                  | Cloud Storage Permissions                                    |
| --------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **Viewer** (`roles/viewer`) | Grants permission to list buckets in the project; view bucket        metadata when listing (excluding ACLs); and list and get HMAC keys in        the project. | `storage.buckets.list`       `storage.hmacKeys.get`       `storage.hmacKeys.list` |
| **Editor** (`roles/editor`) | Grants permission to create, list, and delete buckets in the project;        view bucket metadata when listing (excluding ACLs); and control HMAC        keys in the project. | `storage.buckets.create`         `storage.buckets.delete`         `storage.buckets.list`         `storage.hmacKeys.*` |
| **Owner** (`roles/owner`)   | Grants permission to create, list, and delete buckets in the project;        view bucket metadata when listing (excluding ACLs); and control HMAC        keys in the project.        Within Google Cloud more generally, principals with          this role can perform administrative tasks such as          changing principals' roles for the project or changing billing. | `storage.buckets.create`         `storage.buckets.delete`         `storage.buckets.list`         `storage.hmacKeys.*` |

### Service Key

GCP provides many methods to authenticate, one of the most common and easy is using service keys. It can be created in GCP IAM module.

## Lights Sound & Action

Now armed with the theory, lets start looking at the code, which we can use to leverage basic operations of Cloud Storage in GCP.

### Boilerplate Code

Lets create the basic code upon which we will be using in the entire document. This will include modules such as authentication etc. 

#### With Service key

If service key is to be used for authenticating with GCP then below code can be used as starting point.

```python
import os
from dotenv import load_dotenv
from google.cloud import storage

def get_storage_client(service_key=None):

    if service_key:
        storage_client = storage.Client.from_service_account_json(service_key)
    else:
        storage_client = storage.Client()

    return storage_client

def load_env(env_file):
    load_dotenv(env_file)


def setup(env_file="qa.env", service_key=True):
    if service_key:
        load_env(env_file)
	else:
		load_env()
    service_key = os.environ.get('GOOGLE_APPLICATION_CREDENTIALS')
    return get_storage(service_key)

def main(storage_client):
    pass

if __name__ == "__name__":
	env_file = "qa.env"
	service_key = True
    client = setup(env_file, service_key)
    main(client)
```

Sample `qa.env` file 

```ini
GOOGLE_APPLICATION_CREDENTIALS=/home/mayank/apps/keys/mz001.json
PROJECT_ID=cloudstorage-test-123456
```

 Please **note**, if we are populating `GOOGLE_APPLICATION_CREDENTIALS` environment variable, then we don't need the JSON file path and default client should do the trick as shown in below code

```python
import os
from dotenv import load_dotenv
from google.cloud import storage


def load_env(env_file):
    load_dotenv(env_file)


def setup(env_file="qa.env"):
    load_env(env_file)
    return storage.Client()

def main(client):
    bucket_name = "mayankdemobucket"
    """
    use the client obtained as argument.
    """

if __name__ == "__main__":
    env_file = "qa.env"
    client = setup(env_file)
    main(client)
```

Also,  to access public data, an unauthenticated client can be created using the following code instead.

```python
storage_client = storage.Client()
```

#### Timeouts 

By default, client has a default timeout for non-streaming API calls such as `Create` & `GET`, but it might  not suitable for every use-case, thus we can provide our custom timeout as shown in the below code

```python
def upload_blob(bucket_name, source_file_name, destination_blob_name):
  
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_filename(source_file_name, timeout=300)

    print(
        "File {} uploaded to {}.".format(
            source_file_name, destination_blob_name
        )
    )
```

### Creating Buckets

```python
import os
from dotenv import load_dotenv
from google.cloud import storage

def get_storage_client(service_key=None):

    if service_key:
        storage_client = storage.Client.from_service_account_json(service_key)
    else:
        storage_client = storage.Client()

    return storage_client

def load_env(env_file):
    load_dotenv(env_file)


def setup(env_file="qa.env", service_key=True):
    if service_key:
        load_env(env_file)
    service_key = os.environ.get('TYPE')
    return get_storage_client(service_key)

def create_bucket(client, name, storage_class, location="us"):
    bucket = client.bucket(name)
    bucket.storage_class = storage_class
    print(f"Creating bucket {name}")
    new_bucket = client.create_bucket(bucket, location=location)
    return new_bucket

def main(client):
    # bucket_name = "MayankDemoBucket"
    bucket_name = "mayankdemobucket"
    bucket = create_bucket(client, bucket_name, "COLDLINE")
    print(bucket.name, bucket.location, bucket.storage_class )

if __name__ == "__main__":
    env_file = "qa.env"
    service_key = True
    client = setup(env_file, service_key)
    main(client)
```

### Listing Buckets

`client.list_buckets` returns the collection with all the buckets details present in the project `projectID`

```python
def list_buckets(storage_client):
    for bucket in storage_client.list_buckets():
        yeild bucket.name
```

We can also view public buckets by creating a storage client using `create_anonymous_client` function as shown in the below code. 

>  **Note**: 
>
> Project Id should be provided in the `list_buckets` function as in anonymous_client will not have `project_id` associated with it. Failing to provide it will result in error. 

### Deleting Buckets

Once we delete a bucket,  all the objects stored in the bucket are also deleted and cannot be recovered. Thus delete the bucket with caution. 

```python
def delete_bucket(storage_client, bucket_name):
	try:
    	bucket = storage_client.get_bucket(bucket_name)
    	bucket.delete()
    	print(f"Deleted {bucket.name}")
	except Exception as e:
		print(f"Error: {e}")
		return False
	return True
```

### Move and rename buckets

GCP API's do not directly support move or rename, we need to create a new bucket, then copy all object from the original bucket to new bucket and then delete older bucket.

### Get bucket information

Bucket information is stored in its metadata which can be read as bucket attributes as shown in the below code.

```python
def bucket_info(storage_client, bucket_name):
    try:
        bucket = storage_client.get_bucket(bucket_name)
    except Exception as e:
        print(f"Sorry not able to find the bucket: {bucket_name}, Err: {e}")
        return
    # bucket = storage_client.lookup_bucket(bucket_name)
    if bucket:
        print(f"ID: {bucket.id}")
        print(f"Name: {bucket.name}")
        print(f"Storage Class: {bucket.storage_class}")
        print(f"Cors: {bucket.cors}")
        print(f"Labels: {bucket.labels}")
        print(f"Location: {bucket.location}")
        print(f"Location Type: {bucket.location_type}")
        print(f"Metageneration: {bucket.metageneration}")
        print(f"Default KMS Key Name: {bucket.default_kms_key_name}")
        print(f"Retention Period: {bucket.retention_period}")
        print(f"Retention Policy Locked: {bucket.retention_policy_locked}")
        print(f"Retention Effective Time: {bucket.retention_policy_effective_time}")
        print(f"Self Link: {bucket.self_link}")
        print(f"Time Created: {bucket.time_created}")
        print(f"Requester Pays: {bucket.requester_pays}")
        print(f"Versioning Enabled: {bucket.versioning_enabled}")
        print(f"Default Event Based Hold: {bucket.default_event_based_hold}")
        print(f"Public Access Prevention:"
               " {bucket.iam_configuration.public_access_prevention}")
    else:
        print("Sorry not able to find the bucket")

```

Output:

```python
ID: kmsgbzyuug
Name: kmsgbzyuug
Storage Class: NEARLINE
Cors: []
Labels: {}
Location: ASIA
Location Type: multi-region
Metageneration: 2
Default KMS Key Name: None
Retention Period: None
Retention Policy Locked: None
Retention Effective Time: None
Self Link: https://www.googleapis.com/storage/v1/b/kmsgbzyuug
Time Created: 2022-03-08 02:41:44.132000+00:00
Requester Pays: False
Versioning Enabled: False
Default Event Based Hold: None
Public Access Prevention: {bucket.iam_configuration.public_access_prevention}
```

### Change Storage Class

We can use `bucket.patch` to update bucket metadata a 

Options available:

```python
'STANDARD', 'NEARLINE', 'COLDLINE', 'ARCHIVE', 'MULTI_REGIONAL', 'REGIONAL', 'DURABLE_REDUCED_AVAILABILITY'
```

**Sample Code**

 ```python
 from google.cloud import storage
 from google.cloud.storage import constants
 """
 .....
 """
 
 def change_default_storage_class(storage_client, bucket_name):
     bucket = storage_client.get_bucket(bucket_name)
     bucket.storage_class = constants.COLDLINE_STORAGE_CLASS
     bucket.patch()
     print(bucket_name, bucket.storage_class)    
 ```

### Labels

Labels are collection of key/value pair information stored in buckets metadata and can be accessed using its meta data. 

#### Listing Labels

```python
def list_bucket_labels(storage_client, bucket_name):
    bucket= storage_client.lookup_bucket(bucket_name)
    if bucket:
        print(f"bucket labels: {bucket.labels}")
        for label in bucket.labels:
            print(label)
    else:
        print("Sorry not able to find the bucket")
```

#### Add/Update Label

```python
def add_bucket_labels(storage_client, bucket_name, key, value):
    bucket= storage_client.lookup_bucket(bucket_name)
    if bucket:
        labels = bucket.labels
        labels[key] = value
        bucket.labels = labels
        bucket.patch()

    else:
        print("Sorry not able to find the bucket")
```

Please note that both, key & value should be of lower case and follow all the rules described for bucket name.

#### Delete Label

In the below code, we observe that label can be deleted similar to dictionary.  

```python
def remove_bucket_label(storage_client, bucket_name, label_name):
    """Remove a label from a bucket."""
    bucket = storage_client.get_bucket(bucket_name)
    labels = bucket.labels

    if label_name in labels:
        del labels[label_name]
        
    bucket.labels = labels
    bucket.patch()
```

### Upload File

Uploading any file to bucket is a a multi step process is very simple in python as shown in the below example

```python
def upload_file(storage_client, bucket_name, src_file, dst_loc):
    bucket = storage_client.get_bucket(bucket_name)
    file_name = os.path.basename(src_file)
    dst_loc = "{dst_loc}/{file_name}"
    blob = bucket.blob(dst_loc)
    blob.upload_from_filename(src_file)
```
#### Uploading file using memory data

```python
def upload_file_using_string(storage_client, bucket_name, src_data, dst_file):
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(dst_file)
    blob.upload_from_string(src_data)
```

#### Uploading the file with custom provided encryption key

```python
from google.cloud.storage import Blob

client = storage.Client(project="my-project")
bucket = client.get_bucket("my-bucket")
encryption_key = "aa426195405adee2c8081bb9e7e74b19"
blob = Blob("secure-data", bucket, encryption_key=encryption_key)
with open("my-file", "rb") as my_file:
    blob.upload_from_file(my_file)
```

>  **Note**:
>
> The `encryption_key` should be of at least have length of **32** . 

#### Uploading large files using resumable session

> **Note**: 
>
> Resumable uploads occur automatically when the file is larger than 8 MiB. Alternatively, you can use [Resumable Media](https://googleapis.dev/python/google-resumable-media/latest/resumable_media/requests.html#resumable-uploads) to manage resumable uploads on your own.

### Download File

We can download file using the technique shown in the following code sample

#### Download to Filename

```python
def download_file(storage_client, bucket_name, obj_name, dst_file):
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(obj_name)
    blob.download_to_filename(dst_file)
```

#### Download to Bytes (`download_as_bytes`)

```python
contents = blob.download_as_bytes()
```

#### Download to String (`download_as_text`)

```python
contents = blob.download_as_text()
```

#### Download a portion of an object 

```python
blob.download_to_filename(destination_file_name, start=start_byte, end=end_byte)
```

### Check if file exists (`exists`)

```python
blob = bucket.blob(bucket_filename)
if blob.exists():
    print("blog exists")
```

### Copy File (`copy_blob`)

`copy_blob` can be used to copy file from one location to another location as shown in below code

```python
from google.cloud import storage

client = storage.Client()
bucket = client.bucket("source-bucket")
dst_bucket = client.bucket("destination-bucket")
blob = bucket.blob("src-location/file.ext")
new_blob = bucket.copy_blob(blob, dst_bucket)
new_blob.acl.save(blob.acl)
```

We can even set the destination location using the following code

```python
from google.cloud import storage

client = storage.Client()
bucket = client.bucket("source-bucket")
dst_bucket = client.bucket("destination-bucket")
blob = bucket.blob("src-location/file.ext")
destination_blob_name = "dest-location/file.ext"
new_blob = bucket.copy_blob(blob, dst_bucket, destination_blob_name)
new_blob.acl.save(blob.acl)
```

### Rename blob (`rename_blob`)

```python
new_name = "processed_001.txt"
blob_name = "to_process_001.txt"
blob = bucket.blob(blob_name)
new_blob = bucket.rename_blob(blob, new_name)
```



### Delete Files (`delete_blob`)

```
blob_name = "backup/tobedeleted/backup.tar.gz"
bucket.delete_blob(blob_name)
```

#### Delete Multiple Files (`delete_blobs`)

```python
blobs = [bucket.blob("blob-name-1"), bucket.blob("blob-name-2")]
bucket.delete_blobs(blobs, if_generation_match=if_generation_match)
```

### Listing Files

We can list all files in the bucket using the following code

```python
def list_files(storage_client, bucket_name):
    bucket = storage_client.get_bucket(bucket_name)
    for obj in  storage_client.list_blobs(bucket_name):
        print(obj.name)
```

but most of the time, we don't want all the files and want to view files from a particular folder and its sub folder, in that case we can use `prefix` to denote the folder. 

```python
def list_files_in_folder(storage_client, bucket_name, prefix=""):
    bucket = storage_client.get_bucket(bucket_name)
    for obj in  storage_client.list_blobs(bucket_name, prefix=prefix):
        print(obj.name)
```

We can also restrict the list of files only in the specified folder  by setting `delimiter` value to `/` as shown in below code

```python
def list_files_in_folder(storage_client, bucket_name, prefix="", delimit=""):
    bucket = storage_client.get_bucket(bucket_name)
    for obj in  storage_client.list_blobs(bucket_name, 
            prefix=prefix,
            delimiter=delimit):
        yeild(obj.name)
```

### Metadata

#### View metadata

```python
    bucket = storage_client.bucket(bucket_name)

    # Retrieve a blob, and its metadata, from Google Cloud Storage.
    # Note that `get_blob` differs from `Bucket.blob`, which does not
    # make an HTTP request.
    blob = bucket.get_blob(blob_name)

    print("Blob: {}".format(blob.name))
    print("Bucket: {}".format(blob.bucket.name))
    print("Storage class: {}".format(blob.storage_class))
    print("ID: {}".format(blob.id))
    print("Size: {} bytes".format(blob.size))
    print("Updated: {}".format(blob.updated))
    print("Generation: {}".format(blob.generation))
    print("Metageneration: {}".format(blob.metageneration))
    print("Etag: {}".format(blob.etag))
    print("Owner: {}".format(blob.owner))
    print("Component count: {}".format(blob.component_count))
    print("Crc32c: {}".format(blob.crc32c))
    print("md5_hash: {}".format(blob.md5_hash))
    print("Cache-control: {}".format(blob.cache_control))
    print("Content-type: {}".format(blob.content_type))
    print("Content-disposition: {}".format(blob.content_disposition))
    print("Content-encoding: {}".format(blob.content_encoding))
    print("Content-language: {}".format(blob.content_language))
    print("Metadata: {}".format(blob.metadata))
    print("Medialink: {}".format(blob.media_link))
    print("Custom Time: {}".format(blob.custom_time))
    print("Temporary hold: ", "enabled" if blob.temporary_hold else "disabled")
    print(
        "Event based hold: ",
        "enabled" if blob.event_based_hold else "disabled",
    )
    if blob.retention_expiration_time:
        print(
            "retentionExpirationTime: {}".format(
                blob.retention_expiration_time
            )
        )

```

### Generate Signed URL using  `generate_signed_url`

```python
def create_signed_url(storage_client,
                      bucket_name,
                      bucket_filename,
                      keyfile):
    bucket = storage_client.lookup_bucket(bucket_name)
    if bucket:
        blob = bucket.blob(bucket_filename)
        if blob.exists():
            print("Lets create signed_url")
            expiration = timedelta(3) # valid for 3 days
            credentials = service_account.Credentials.from_service_account_file(keyfile)
            expiration = timedelta(3) # valid for 3 days
            url = blob.generate_signed_url(expiration, method="GET",
                                           credentials=credentials)
            return url
    else:
        print("Sorry not able to find the bucket")
        return -1
```

## Composite Objects

Composite objects are created by using existing objects on the bucket without uploading any additional data. The use case of them is

- Creating large object from smaller objects which were uploaded in parallel.
- Creating unified view from existing multiple object, useful for viewing unified log from multiple log files.

### Compose operation

Compose operation concatenates data from multiple listed objects in sequence and it creates a composite objects. Main requirements of source objects

-  Same storage class
- Same cloud storage bucket

The operation will not affect the source files/blobs. Also note that final composite object will also have the same storage class and bucket.

Also note, that after creating the composite object, if any source file is updated, even then it will not be updated.

```python
def composite_obj(storage_client, bucket_name, dest_blob_name, files):
    bucket = storage_client.lookup_bucket(bucket_name)
    if bucket:
        destination = bucket.blob(dest_blob_name)
        destination.content_type = "text/plain"
        sources = []
        for file in files:
            sources.append(bucket.get_blob(file))
        destination.compose(sources)
    else:
        print("Sorry not able to find the bucket")

def main(client):
    bucket_name = "kmsgbzyuug"
    files = ['test/tdd/copyfile.py', 'dummy/generate.vim']
    dest_file = "test/final.txt"
    composite_obj(client, bucket_name, dest_file, files)

```

Please note, If the blobs are not of same storage class then following error will be observed.

```python
Cannot compose object with destination storage class COLDLINE from source components that have different storage classes (NEARLINE)
```

## Object Lifecycle

Cloud storage provides us facility to control the life cycle using _retention policy_. Using this policy, a retention period can be specified, during which the asset can not be deleted or replaced. 	

The object life cycle can be applied on the entire bucket, thus its automatically applied on all the files present in the bucket. 

One of the features in Object Life cycle is Object Versioning, which allows to have multiple versions of the same object out of which one version will be live version.

Object versioning can be enabled on a bucket using the following code.

  ```python
  def bucket_versioning(storage_client, bucket_name):
      bucket = storage_client.lookup_bucket(bucket_name)
      if bucket:
          print(f"Versioning Enabled: {bucket.versioning_enabled}")
          # Enabling the versioning
          bucket.versioning_enabled = True
          bucket.patch()
          print(f"Versioning Enabled: {bucket.versioning_enabled}")
          # Disabling the versioning
          bucket.versioning_enabled = False
          bucket.patch()
          print(f"Versioning Enabled: {bucket.versioning_enabled}")
      else:
          print("Sorry not able to find the bucket")
  ```

### Accessing non-current version

Python module do not have any method to access non-current version, and can use the following method to access them. 

URL Path

```python
https://storage.googleapis.com/storage/v1/b/BUCKET_NAME/o/OBJECT_NAME?generation=GENERATION_NUMBER
```

**Sample code**:

```python



ADD CODE WITH AUTHENTICATION FOR UPDATING HEADER
================================================


$$TODO$$



```

 ### Restore non-current object version

We can restore a non current version of object by making a copy of it, converting it current version and existing current version will become non-current version as shown in the below code.

```python
def activate_version_file(storage_client, bucket_name, file_path, generation):
    
    source_bucket = storage_client.bucket(bucket_name)
    source_blob = source_bucket.blob(file_path)

    blob_copy = source_bucket.copy_blob(
        source_blob, source_bucket, file_path, source_generation=generation
    )
```

> **Note**:
>
> By restoring noncurrent version, our code will create a new copy of the non-current version as current version and existing non-current will continue to exist. 

### Deleting non-current object version

```python
def delete_file_archived_generation(storage_client,
        bucket_name, blob_name, generation):
    bucket = storage_client.get_bucket(bucket_name)
    bucket.delete_blob(blob_name, generation=generation)
```

### Retention policies

GCP allows to setup a retention polity at bucket level which provides a mechanism to enforce the corporate retention policies. Once set no item can be deleted from that bucket on which the policy has not expired.

The policy 

- is retroactively applied on existing objects
- is applied automatically on new objects
- once applied, will not allow the objects to be deleted for whom the age of object is lesser then that set in retention policy.
- will not be effect the meta-data's of the object and editable meta-data can still be updated. 
- contains "effective time" after which all the object in the bucket are automatically complaint with the policy. 

The Policy can also be permanently applied on the bucket which

-  will disallow the bucket to be deleted if even one object's age has not crossed that set in retention policy. 
- allows the retention time to be increased but not decreased
- will not allow the retention policy to be deleted

 "retention expiration time" meta-data can be used to view time left before retention policy is fulfilled on the object. 

